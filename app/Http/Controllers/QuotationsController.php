<?php

namespace App\Http\Controllers;

use App\Models\QuotationsModel;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class QuotationsController extends Controller
{
    public function version_actual(Request $request){
        $version = QuotationsModel::where("code",$request->code)->first();
        $actual_version = $this->buscar_version_final($version->id);
        return array("version_actual"=>!is_null($actual_version)?$actual_version:$version);

    }

    public function buscar_version_final($id){
        $versionBuscar =  QuotationsModel::where("original_id",$id)->first();
        if(!is_null($versionBuscar)){
            $versionBuscar = $this->buscar_version_final($versionBuscar->id);
        }else{
            $versionBuscar =  QuotationsModel::where("id",$id)->first();
        }
        return $versionBuscar;
    }

    public function primera_version(Request $request){
        $version = QuotationsModel::where("code",$request->code)->first();
        if(!is_null($version->original_id)){
            $version = $this->buscar_version_inicial($version->original_id);
        }
        return $version;
    }

    public function buscar_version_inicial($id){
        $versionIni = QuotationsModel::where("id",$id)->first();
        if(!is_null($versionIni->original_id)){
            $versionIni = $this->buscar_version_inicial($versionIni->original_id);
        }
        return $versionIni;
    }



}
